import express from 'express';
import postRouter from './router/post.js';
import userRouter from './router/user.js';
const app = express();


// 반복되는 느낌이 있음. 이 부분을 갆편하게 할려면 ROUTE를 사용하면 됨. 
/*
app.get('/posts', (req, res) => {
    res.status(201).send('GET: /posts');
})
app.post('/posts', (req, res) => {
    res.status(201).send('POST: /posts');
})
*/

// ROUTE를 사용하면 간편함. 
// BUT, 복잡한 서버는 여러가지 경로가 존재해서, APP.JS 한곳에서 나열하면 유지보수 측면에서 어려움.
/*
app.route('/posts',)
.get((req, res, next) => {
    res.status(201).send('GET: /posts');;
})
.post((req, res) => {
    res.status(201).send('POST: /posts');
})
*/

app.use(express.json()); // REST API -> Body
app.use(express.urlencoded({extended: false})); // HTML Form -> Body
const options = {
    dotfiles: 'ignore',
    etag: false,
    index: false,
    maxAge: 'id',
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set('x-timestamp', Data.now());
    },
};

app.use(express.static('public', options)); // public폴더 안에 있는 파일을 사용자가 요청시 바로 응답해줄 수 있음. 리소스에 접근이 가능. 

// 큰 도메인별로 구분하는게 가독성이 좋고 유지보수 하기 편함.
// 제일 상위에 있는 경로를 지정후 라우터를 연결
app.use('/posts', postRouter);
app.use('/users', userRouter);



app.listen(8080);

