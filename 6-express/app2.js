import express from 'express';
import fs, { fstatSync } from 'fs';
//const fs = require('fs');
//import fsAsync from 'fs/promises';
//const os = require('os');
//const fsAsync = require('fs/promises');
import { deepStrictEqual } from 'assert';

const app = express();

app.use(express.json());


app.get('/file', (req, res) => {
    fs.readFile('/file1.txt', (err, data) => {
        if (err) {
            console.error();
            res.sendStatus(404);
        };
    });
});

app.get('/file1', (req, res) => {
    // 이대로 실행시키면, 클라이언트 화면에 에러메시지 노출
    // const data = fs.readFileSync('/file1.txt');
    // res.send(data);

    // 적절한 에러메시지를 보냄.
    try {
        const data = fs.readFileSync('/file1.txt');
        res.send(data);
    } catch (error) {
        res.sendStatus(404);
    }
});

// promises
app.get('/file2', (req, res) => {
    
});

// async
app.get('/file3', async (req, res) => {
    
});


app.listen(8080);

