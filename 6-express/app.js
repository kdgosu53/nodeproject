import express from 'express';
const app = express();

/*
app.get('/sky/:id', (req, res, next) => {
    //console.log(req.path);
    //console.log(req.headers);
    console.log(req.params);
    console.log(req.params.id);

    console.log(req.query);
    console.log(req.query.keyword);
    
    res.setHeader('key', 'value');
    //res.send('hi!');
    //res.json({ name: 'Ellie '});
    //res.sendStatus(201);
    res.status(201).send('created');
});
*/




// 콜백함수는 순서가 중요하다!!
// 콜백함수는 한 경로에 대해서 배열형태로 여러가지를 등록할 수 있다. 
// 



app.use(express.json());


// app.all 과 app.use 의 차이

// all은 지정한 경로에 한해서만, http request가 수행이 됨. localhost:8080/api/test -> 실행이 안됨. 
// /api/*/ 모든 것에 대해서 명시적으로 해줘야 실행됨. 
app.all('/api', (req, res, next) => {
    console.log('all');
    next();
});

// all과 반대로 이어지는 어떤 경로에 대해서도 수행이 됨. 
app.use('/sky', (req, res, next) => {
    console.log('use');
    next();
})



app.get(
    '/', 
    (req, res, next) => {
        console.log('first');
        // res나 next 에 아무것도 호출을 안해주면, 서버는 동작을 안함. 중지된 상태. 
        // next(); // 다음 경로로 이동함. 
        // next('route'); // 다음 미들웨어로 이동
        // next(new Error('error')); // 에러메시지 출력. 에러를 처리하는 부분을 달아줘야 됨. 사용자에게 에러를 보내면 안됨. 
        // res.send('Hello');  // send 시 다음 부분 호출이 안됨. 

        // 한 콜백함수에서 두번씩 res를 보내게 되면 에러가 발생!
        // 그래서 return을 붙여줘서 이 함수가 끝나도록 만들어야 됨. 
        if(true) {
            //return res.send('Hello');
        }
        //res.send('Ellie');

        next();
    }, 
    (req, res, next) => {
        console.log('first2');

        // post로 json형태로 데이터를 받을 시, express에서 제공하는 api로 받으면 됨.(post)
        console.log(req.body);
        next();
    }
);

app.get('/', (req, res, next) => {
    console.log('second');
    res.send('asdada');
});

// 아무거도 호출이 안되어있을 시.  Cannot GET /doc
app.use((req, res, next) => {
    console.log('not found')
    res.status(404).send('Not Found! @_@');
})

// 이렇게 설정을 해놓으면, 중간중간에 에러가 일어나더라도 제일 마지막에 있는 미들웨어가 에러에 대한 처리를 함. 
app.use((error, req, res, next) => {
    console.error(error);
    res.status(500).send('Sorry, try later!!')
})

app.listen(8080);

