import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';  // 쿠키정보를 가져옴
import morgan from 'morgan';  // 사용자에게 요청을 받을때마다의 유용한 정보를 자동으로 해줌 , 서버에 어떤 요청이 왓는지 모니터링 좋음.
import helmet from 'helmet';  // 공통적으로 보완에 필요한 header들을 추가해줌. 


const app = express();


app.use(express.json());
app.use(cookieParser());
app.use(morgan('tiny'));
app.use(helmet());


// 일일히 이렇게 해주기는 귀찮. 오타가 날 확률도 있음.
// app.use((req, res, next) => {
//     res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5500');
//     res.setHeader(
//         'Access-Control-Allow-Methods',
//         'OPTIONS, GET, POST, PUT, DELETE'
//     );
//     next();
// })

app.use(
    cors({
        //origin: ['http://127.0.0.1:5500'], // 지정한 IP에서만 CORS를 허용하도록 설정.
        //optionsSuccessStatus: 200,
        //credentials: true, // Access-Control-Allow-Credentials: true 와 동일./
    })
);



app.get('/', (req, res) => {
    console.log(req.body);
    console.log(req.cookies);
    console.log('////')
    req.cookies.a_cookie; // 전달받은 쿠키에 접근.
    //res.send('Welcomes!');
    res.write('data');
    res.end('Jello World')
});

app.listen(8080);