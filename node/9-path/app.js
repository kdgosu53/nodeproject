// 운영체제별로 경로 표현법이 달라져도, 잘 동작할 수 잇게 함
const path = require('path');


// POSIX (Unix: Mac, Linux): 'Users/temp/myfile.html'
// Window: 'C:\\temp\\myfile.html

// 글로벌 객체에 잇는 디렉토리 
console.log(__dirname);
console.log(__filename);

console.log(path.seq);
console.log(path.delimiter);

// basename
console.log(path.basename(__filename));
console.log(path.basename(__filename, '.js'));

// dirname
console.log(path.dirname(__filename));

// extension 확장자
console.log(path.extname(__filename));

//parse
const parsed = path.parse(__filename);
console.log(parsed);
console.log(parsed.root);
console.log(parsed.name);

const str = path.format(parsed);
console.log(str);

// isAbsolute
console.log('isAbsolute?', path.isAbsolute(__dirname)); // /Users/...   절대경로
console.log('isAbsolute?', path.isAbsolute('../'));     // 상대경로

// normalize 
// 경로가 이상하거나 잘못된 부분이 있으면, 알아서 고쳐준다. 
console.log(path.normalize('./folder//////////sub'));

// join!
// 디렉토리를 추가
console.log(__dirname + '/' + 'image');

console.log(path.join(__dirname, 'image'));