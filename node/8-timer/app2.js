
console.log('code1');
console.time('timeout 0'); // 콜스택이 텅텅 빌때까지 기다려야 되므로 시간이 일정하지 않다. 
setTimeout(() => {
    console.timeEnd('timeout 0');
    console.log('setTimeout 0')
}, 0);

console.log('code2');
setImmediate(() => {
    console.log('setImmediate');

});

console.log('code3');

process.nextTick(() => {
    console.log('nextTick')
})

try {

} catch (error) {

}

