

let aa = 10;

// 함수안에서 this를 호출하면 global이다.!
function hello() {
    console.log(this === global);
    console.log(aa);
    console.log(this.aa);
}

hello();


class A {
    constructor(num) {
        this.num = num;
    }

    memberFunction() {
        console.log('------ class ------');
        console.log(this);      // class 자체를 가르킴 
        console.log(this === global);  
    }
}

const a = new A(1);
a.memberFunction();


console.log(this);
console.log('--- global scope --- ');
console.log(this);
console.log(this === module.exports);    // 브라우저에서는 글로벌을 가르킴 


