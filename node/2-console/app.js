console.log('logging...');
console.clear();

// log level 나중에 배포 시 서버에 심각성을 빠르게 알아차리기 쉽고, 수정이 쉬움. 
// 배포 시 log를 출력할것인지 아닌지, 컨트롤할 수 있다. 
// 
console.log('log'); // 개발
console.info('info'); // 정보
console.warn('warn'); // 경보
console.error('error'); // 에러, 사용자 에러, 시스템 에러

// assert 전달값이 true가 아닐때만 출력. (false일때)
console.assert(2 === 3, 'not same!');
console.assert(2 === 2, 'same!');

// print object
const student = {
    name: 'ellie',
    age: 20,
    company: {
        name: 'AC'
    }
}
console.log(student);
console.table(student);   // 테이블 형태로 출력
console.dir(student, { showHidden: true, colors: false, depth: 0});   // 중첩된 Object를 어디까지 보여줄껀지 확인. 

// measuring time
// 시간을 지정할 수 있다. 
// 성능을 확인할 때 유용
console.time('for loop');
for(let i = 0; i < 10; i++) {
    i++;
}
console.timeEnd('for loop');

// counting
// 몇번 호출되었는지 확인
function a() {
    console.count('a function');
}
a();
a();
console.countReset('a function'); // 초기화 
a();

// trace
// 디버깅시 이점
// 누가 이 함수를 호출했을 때 알 수 있음.

function f1() {
    f2();
}
function f2() {
    f3();
}
function f3() {
    console.log('f3');
    console.trace();
}

f1();
