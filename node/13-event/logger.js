// 특정한 일을 수행하는 logger
// EventEmitter는 한번 객체를 만들면, 그 객체 내에서 이벤트를 사용할 수 있음. 
// 여러개의 EventEmitter 객체를 만들면, 다른 Emitter에서 이벤트를 사용할 수 없다. 

const EventEmitter = require('events');
const { emit } = require('process');
/*
const emitter = new EventEmitter;

function log(callback) {
    emitter.emit('log', 'started...');
    callback();
    emitter.emit('log', 'ended!');
}
*/

class Logger extends EventEmitter{
    log(callback){
        this.emit('log', 'started...');
        callback();
        this.emit('log', 'ended!');
    }
}

module.exports.Logger = Logger;