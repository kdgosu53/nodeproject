const logger = require('./logger');
//const EventEmitter = require('events');
//const emitter = new EventEmitter;
const emitter = new logger.Logger();

// 동작안됨. 이유는 다른 Emitter객체에서 있던것을 다시 들고왔기 때문에 안됨. 
// 수정
emitter.on('log', (event) => {
    console.log(event);
});



emitter.log(() => {
    console.log('..... doing something!');
})