const os = require('os'); // 

// os에서 줄바꿈 문자열이 어떤것인지..
console.log(os.EOL);
console.log(os.EOL === '\n');          // MAC
console.log(os.EOL === '\r\n');        // WINDOW

console.log(os.totalmem());
console.log(os.freemem());
console.log(os.type());
console.log(os.userInfo());
console.log(os.cpus());
console.log(os.homedir());
console.log(os.hostname());




