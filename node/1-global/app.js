const fs = require('fs');

// 브라우저의 window 객체에 해당
console.log(global);

global.hello = () => {
    global.console.log('hello');
}

global.hello();
hello();                 // global 생략가능