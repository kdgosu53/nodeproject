const process = require('process');

console.log(process.execPath);
console.log(process.version);
console.log(process.pid);
console.log(process.ppid);
console.log(process.platform);
console.log(process.env);         // 컴퓨터에 저장된 환경변수
console.log(process.uptime());
console.log(process.cwd());       // 현재 사용하고 있는 node의 경로
console.log(process.cpuUsage());

// 현재 동작하고 있는 node의 프로세스 정보를 얻을 수 있다. 

setTimeout(() => {
    console.log('setTimeout')
}, 5000);

// 지금은 아닌데, 현재 수행되고 잇는 코드가 다 완료된 다음에, 내가 등록한 callback함수를 테스크큐에 넣어달라고 하는 함수
// 테스크 큐 제일 앞부분에다 먼저 넣어줌. 
process.nextTick(() => {
    console.log('nextTick')
})

for(let i = 0; i < 100; i++ ){
    console.log('for loop');
}



