const fs = require('fs');

// 모든 api는 3가지 제공
// rename(...., callback(error, data)) 비동기
// try { renameSync(....) } catch(e) {}  에러 시 node프로세스가 죽으면 안되니 try catch문으로 감싸줘야됨.  동기
// promises.rename().then().catch(0)

try {
	fs.renameSync('./text.txt', './text-new.txt');
} catch (err) {
	console.log('err1');
	console.log(err)
}

fs.rename('./text-new.txt', './text.txt', (err) => {
	console.log(err);
});
console.log('hello');


fs.promises
	.rename('./text2.txt', './text-new.txt')
	.then(() => console.log('Done!'))
	.catch(() => {
		//console.log('err3');
		console.error
	})
