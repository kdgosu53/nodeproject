const fs = require('fs').promises;

// reading a file
fs.readFile('./text.txt', 'utf8') // 파일이름, 인코딩 // 인코딩을 하지 않으면 Buffer에 있는 내용을 그대로 보여준다. 
    .then(data => console.log(data))
    .catch(console.error);
    
// writing a file
fs.writeFile('./file.txt', 'Hello, Dream Coders! :) ')
    .catch(console.error);

// 기본 파일을 덮어씀. 
//fs.writeFile('./file.txt', 'Yo, Dream Coders! :) ')
  //  .catch(console.error);

// 추가
fs.appendFile('./file.txt', 'Yo!, Dream Coders! :) ')
    .then( () => {
        // copy
        fs.copyFile('./file.txt', './file2.txt')
            .catch(console.error);

    })
    .catch(console.error);

// copy
//fs.copyFile('./file.txt', './file2.txt')
  //  .catch(console.error);


// folder
fs.mkdir('sub-folder')
    .catch(console.error);

fs.readdir('./')
    .then(console.log)
    .catch(console.error);



