const fs = require('fs');

// stream은 이벤트 베이스
const readStream = fs.createReadStream('./file.txt', {
    //highWaterMark: 8, // 한번에 얼마만큼의 데이터를 읽어와서 저장하는지  // default 64kb
    //encoding: 'utf-8'
});  // on이벤트를 등록 후 계속 on을 사용 가능함. 


const data = [];
readStream.on('data', chunk => {
    //console.log(chunk);
    console.count('data');
    data.push(chunk);
});

readStream.on('end', () => {
    console.log(data.join(''));
})

readStream.on('error', error => {
    console.log(error);
});

// once 딱 한번만 실행 됨 .

