const fs = require('fs');
const zlib = require('zlib');

const readStream = fs.createReadStream('./file.txt');
const zlibStream = zlib.createGzip();
const writeStream = fs.createWriteStream('./file4.zip');

// stream 과 stream을 연결함
const piping = readStream.pipe(zlibStream).pipe(writeStream);
piping.on('finish', () => {
    console.log('dine!!');
})

// 나중에 서버를 만들때도 유용! 
const http = require('http');

const server = http.createServer((req, res) => {
    // 서버에서는 파일을 다 읽은 다음에 데이터를 반응해서 보내줌. 
    fs.readFile('file.txt', (err, data) => {
        res.end(data);
    });

    // stream을 이용하는게 더 효과적. 
    const stream = fs.createReadStream('./file.txt');
    stream.pipe(res);

    
});

server.listen(3002);

