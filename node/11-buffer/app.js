// Fixed-size chuck of memory   메모리에서 고정된 사이즈의 메모리 덩어리
// array of integers, byte of data  숫자의 배열. 메모리에 있는 데이터 자체~!

const fs = require('fs');
const buf = Buffer.from('Hi');
console.log(buf);  // 유니코드 형태로 표현
console.log(buf.length);
console.log(buf[0]);  // 배열로 출력 시 아스키코드 형태로 출력됨. 
console.log(buf[1]);
console.log(buf.toString());     // toString에는 인코딩을 전달할 수 있음.  기본이 utf-8

// create
const buf2 = Buffer.alloc(2);  // 메모리에서 사용 가능한 버퍼를 찾아서 초기화시킴.
const buf3 = Buffer.allocUnsafe(2);  // 초기화 하지 않음. but 데이터가 들어있을수도 있음. fast
buf2[0] = 72;
buf2[1] = 105;
buf2.copy(buf3);      // 복사 
console.log(buf2);
console.log(buf3.toString());

// concat 여러가지 buffer를 모을 수 있다. 
const newBuf = Buffer.concat([buf, buf2, buf3]).toString();
console.log(newBuf);

