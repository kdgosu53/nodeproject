let count = 0;

function increase() {
    console.log('increase')
    count++;
}

function getCount() {
    return count;
}

module.exports.getCount = getCount;
// module.exports.increase = increase;
console.log(module.exports === exports);
exports = {};                 // 특정한 값을 할당했을 때는 module.exports를 가리키고 있지 않고, {}를 가리킨다.  
console.log(module.exports === exports);
exports.increase = increase;  // exports라는 것은 module.exports를 가리키고 있음. 
console.log(module);
