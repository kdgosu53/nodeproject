import express from 'express';
import { body, param, validationResult } from 'express-validator';

const app = express();
app.use(express.json());

app.get('/test', (req, res) => {
  console.log('test()');
  res.sendStatus(201)
});

const validate = (req, res, next) => {
  const errors = validationResult(req);
    if(errors.isEmpty()) {
      return next();
    } 
    // return res.status(400).json({message: errors.array()[0].msg}); // 첫번째 에러메시지만 받아옴.   
    return res.status(400).json({message: errors.array()});   
};

// name의 글자수가 최소 2글자, 2글자가 아니면 에러.
// withMessage: 커스텀 에러 메시지
app.post(
  '/users', 
  /* body('name')
    .notEmpty() 
    .withMessage('이름을 입력해!')
    .isLength({min: 2})
    .withMessage('이름은 두글자 이상!'),
  body('age')
    .notEmpty()
    .isInt()
    .withMessage('숫자를 입력해!'),  */

    // 배열로도 가능함.
    [                                                 // 빈 공백값을 제거한다. 
      body('name').notEmpty().withMessage('이름을 입력해!').trim().isLength({min: 2}).withMessage('이름은 두글자 이상!'),
      body('age').notEmpty().isInt().withMessage('숫자를 입력해!'),
      body('email').isEmail().withMessage('이메일 입력해요').normalizeEmail(), // normalizeEmail : 대문자로 된 영어를 소문자로 변경함. 
      body('job.name').notEmpty(),
      validate
    ],
    
    (req, res, next) => {
  //validate(req, res, next);
  // const errors = validationResult(req);
  // if(!errors.isEmpty()) {
  //   return res.status(400).json({message: errors.array()});
  // }

  console.log(req.body);
  res.sendStatus(201);

    // express-validator 이 없다면 일일이 하나하나씩 유효성 검사를 해야됨.
    // if(req.body.email...) {
    //   res.status(400).send({message: 'email!!!!'})
    // } else if() ...

    //res.sendStatus(201);
  }
);

app.get(
  '/:email', 
  [param('email').isEmail().withMessage('이메일 입력해요'),
  validate],
  (req, res, next) => {
    // const errors = validationResult(req);
    // if(!errors.isEmpty()) {
    //   return res.status(400).json({message: errors.array()});
    // } 
    //res.send('💌');
  }
);

app.listen(3300);
