console.log('app.js');

const process = require('process');
const path = require('path');
const fs = require('fs');

const basePath = path.join(__dirname, '../../../Lee/Pictures');
let imgPath = null;
let FileList = null;


try {
    imgPath = path.join(basePath, process.argv[2]);    
} catch (error) {   
    console.log(error);
}


createFolder();


let test = 'E1234';
let test2 = 'E3546';

if(test.indexOf('E') === 0){
    console.log('good');
}

console.log(process.argv[0]);
process.argv.forEach((val, index) => {
    console.log(`${index}: ${val}`);

});





try {
    //fs.rename(path.join(imgPath, 'IMG_2345.jpg'), path.join(imgPath, 'captured', 'IMG_2345.jpg'), (err) => {
      //  console.log(err);
    //});    
} catch (error) {
    //console.log(error);
}

//console.log('-=-----------')
//console.log(FileList);
//const stream = fs.createReadStream(strea)


function createFolder() {

    // folder
    fs.mkdir(path.join(imgPath, 'video'), (err) => {
        if(err !== null && err.code === 'EEXIST') return console.error('video 폴더가 존재합니다!');
        console.log('Directory created successfully video!');

        fs.mkdir(path.join(imgPath, 'captured'), (err) => {
            if(err !== null && err.code === 'EEXIST') return console.error('captured 폴더가 존재합니다!');
            console.log('Directory created successfully captured!');

            fs.mkdir(path.join(imgPath, 'duplicated'), (err) => {
                if(err !== null && err.code === 'EEXIST') return console.error('captured 폴더가 존재합니다!');
                console.log('Directory created successfully duplicated!');

                fs.readdir(imgPath, (err, filename) => {
                    console.log('readdir');
                    
                    // 배열로 저장됨.
                    console.log(imgPath);
                    console.log(filename);
                    FileList = filename;
                
                    moveFile(FileList);
                
                });

            });
        });
    });
   
   
}

// 파일이동
function moveFile(list) {
    console.log('moveFile');
    console.log(list);

    list.forEach((value, index) => {
        let ArrFileNm = null;
        let fileNm = null;

        if(path.extname(value) === '.mp4' || path.extname(value) === '.mov'){
            fs.rename(path.join(imgPath, value), path.join(imgPath, 'video', value), (err) => {
                if(err !== null && err.code === 'ENOENT') return console.log('video 파일 또는 폴더가 없습니다! ');
                console.log('move video: ' + value);
            });
            

        }else if(path.extname(value) === '.png' || path.extname(value) === '.aae') {
            fs.rename(path.join(imgPath, value), path.join(imgPath, 'captured', value), (err) => {
                if(err !== null && err.code === 'ENOENT') return console.log('captured 파일 또는 폴더가 없습니다! ');
                console.log('move captured: ' + value);
            });
            

        }else if(path.extname(value) === '.jpg') {
            ArrFileNm = value.split('_');
            fileNm = ArrFileNm[1];

            if(fileNm.indexOf('E') !== -1){ 
                let reValue = value.replace('E','');
                fs.rename(path.join(imgPath, reValue), path.join(imgPath, 'duplicated', reValue), (err) => {
                    if(err !== null && err.code === 'ENOENT') return console.log('duplicated 파일 또는 폴더가 없습니다! ');
                    console.log('move duplicated: ' + reValue);
                });
                
            }
        }
    })

}

