import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';


import tweetsRouter from './routes/tweets.js';
import authRouter from './routes/auth.js';

const app = express();
app.use(morgan('tiny'));
app.use(helmet());
app.use(cors());

app.use(express.json()); // REST API -> Body
// app.use(express.urlencoded({extended: false})); // HTML Form -> Body
// const options = {
//     dotfiles: 'ignore',
//     etag: false,
//     index: false,
//     maxAge: 'id',
//     redirect: false,
//     setHeaders: function (res, path, stat) {
//         res.set('x-timestamp', Data.now());
//     },
// };

// 제일 상위에 있는 경로를 지정후 라우터를 연
// app.use('/posts', postRouter);

app.use('/tweets', tweetsRouter);
app.use('/auth', authRouter);

app.use((req, res, next) => {
    res.sendStatus(404)
});

app.use((error, req, res, next) => {
    console.error(error);
    res.sendStatus(500);
})
// app.get('/', (req, res, next) => {
//     res.json({'aa': 'bb'});
// });


app.listen(3300, () => {
    console.log('server running');
});
