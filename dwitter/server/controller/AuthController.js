import authModel from '../data/Auth.js';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

// const jwt = require('jsonwebtoken');




// TODO: Make is secure!
// 이런거는 configuration에 저장한다!
const jwtSecretKey = 't8@^HnEhG%LV&SQq$$LTYQdP!93UXGtG';
const jwtExpriresInDays = '2d';
const bcryptSaltRounds = 12;



function log(data) {
    console.log(`req : ${JSON.stringify(data)}`);
}

function createJwtToken(userId) {
    return jwt.sign({
        id: userId,
        isAdmin: false
    }, jwtSecretKey, 
    {expiresIn: jwtExpriresInDays});
}

export default {
    postLogin: async function(req, res, next) {
        console.log(`postLogin req: ${JSON.stringify(req.body)}`);
        const {username, password} = req.body;

        let result = null;
        let sData = {
            username: '',
        };
        try {
            let body = req.body || '';

            //console.log(`body: ${body}`);
            //sData.username = username;

            const user = await authModel.findByUsername(username);
            if(!user) {
                return res.status(401).json({message: 'Invalid user or password'})                
            }

            const isValidPassword = await bcrypt.compare(password, user.password);
            if(!isValidPassword) {
                return res.status(401).json({message: 'Invalid user or password2'})                
            }
            
            const token = createJwtToken(user.id);
            res.status(200).json({token, username});

            /* 
            let data = await authModel.postLogin(req.body);
            if(data.length > 0) {
                const token = jwt.sign(
                    {
                        id: data[0].username,
                        isAdmin: false,
                    }, jwtSecretKey,
                    { expiresIn: 30 }
                );
                result = {
                    username: data[0].username,
                    token: token                    
                }
                res.status(200).json(result);   
            }
            else res.status(401).json({ message: '아이디나 비밀번호가 틀립니다. '});
             */
            //res.status(200).json(result);
    
        } catch (e) {
            console.error(e);
        }
    },
    postSignUp: async function(req, res, next) {
        let result = null;
        const {username, password, name, email, url} = req.body;
        let sData = {
            username: '',
            password: '',
            name: '',
            email: '',
            url: '',
        };
        try {
            log(req.body);
            sData.username = req.body.username;
            sData.password = req.body.password;
            sData.name = req.body.name;
            sData.email = req.body.email;
            sData.url = req.body.url;
            
            let found = await authModel.findByUsername(username);
            // username 중복 체크
            if(found) {
                return res.status(409).json({message: `${username} already exists! `})
            }

            const hashed = await bcrypt.hash(password, bcryptSaltRounds);
            const userId = await authModel.createUser({
                username,
                password: hashed,
                name,
                email,
                url
            });

            const token = createJwtToken(userId);
            res.status(200).json({token, username})

            /* const token = jwt.sign(
                {
                    id: userId,
                    isAdmin: false,
                }, jwtSecretKey,
                { expiresIn: 30 }
            );
            result = {
                username: sData.username,
                token: token                    
            }
            res.status(200).json(result);    */
            
            
        } catch (e) {
            console.error(e);
        }
    },
    me : async function(req, res, next) {
        const user = await authModel.findById(req.userId);
        if(!user) {
            return res.status(404).json({message: 'User not found'});
        }
        res.status(200).json({token: req.token, username: user.username});
    },

    
}










