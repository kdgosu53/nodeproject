import express from 'express';
import dataModel from '../data/data.js';


export default {
    getTweets: async function(req, res, next) {
        let result = null;
        let sData = {
            username: '',
        };
        try {
            console.log('111111111');
            let username = req.query.username || '';
            
            console.log('username : ', username);
            sData.username = username;

            result = await dataModel.getTweets(sData);
            
            console.log('result : ', result);
            res.status(200).json(result);
    
        } catch (error) {
            console.log('error: ', error);
        }
    },
    getTweet: async function(req, res, next) {
        let result = null;
        let sData = {
            username: '',
        };
        try {
            console.log('111111111');
            let username = req.query.username || '';
            
            console.log('username : ', username);
            sData.username = username;

            result = await dataModel.getTweet(sData);
            
            console.log('result : ', result);
            res.status(200).json(result);
    
        } catch (error) {
            console.log('error: ', error);
        }
    },
    postTweets: async function(res, req, next) {
        console.log('post - tweets');
        //console.log(req);
        console.log(req.req.body);

        // solution
        let result = null;

        try {
            let sData = req.req.body;
            result = await dataModel.postTweets(sData);
            console.log(`result : ${result}`);
            res.res.status(201).json(result);

        } catch (error) {
            console.error(error);
        }
    },

    updateTweets: async function(req, res, next) {
        console.log('update - tweets');

        let tweet = req.body;
        let userId = req.params.id;
        let result = null;

        let sData = {
            tweet: tweet,
            userId : userId
        }
        try {
            console.log(tweet);
            console.log(userId);
            result = await dataModel.updateTweets(sData);
            console.log(`result : ${JSON.stringify(result)}`);
            res.status(201).json(result);

        } catch (error) {
            console.error(error);
        }

        /* console.log(`put tweet : ${JSON.stringify(tweet)}`);
        console.log(`put userId : ${userId}`);
        let data = tweets.find((t) => {
            if(Number(t.id) === Number(userId)) {
                t.text = tweet.text;
                return t;
            }
        });
        console.log(`data : ${data}`);
        res.status(201).json(data); */
    },

    deleteTweets: async function(req, res, next) {
        console.log('delete - tweets');

        let userId = req.params.id;
        let result = null;
        
        let sData = {
            userId: userId
        }
        try {
            result = await dataModel.deleteTweets(sData);
            res.status(204).json(result);
        } catch (error) {
            console.error(error);
        }
    }

    

    
}




// let template = { a: 'a', b: 'b' }
let template = 'test';
/* export async function test (req, res) {
    template;
} */
// module.exports = template;

/* export default async function aa (a) {
    return 'test';
} */

/* async function bb () {
    return 'test';
} */

// export default aa;

/* export async function aa () {
    return 'test';
} */

/* module.exports = {
    aa: async function () {
        return 'test';
    }
} */













