
let tweets = [
    {
        id: 1,
        text: '드림코딩에서 강의 들으면 너무 좋으다',
        createdAt: Date.now().toString(),
        name: 'ellie',
        username: 'ellie',
        url: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-1.png',
    },
    {
        id: 2,
        text: '코딩',
        createdAt: Date.now().toString(),
        name: 'bob',
        username: 'bob'
    },
]

export default {
    getTweets: async function(sData) {

        try {
            let username = sData.username;
            const data = username 
            ? tweets.filter((t) => t.username === username)
            : tweets;
            
            return data;
        } catch (error) {
            console.error(error);
        }
    },
    getTweet: async function(sData) {
        try {
            console.log('data - getTweet () ');
            let username = sData.username;
            const data = username 
            ? tweets.filter((t) => t.username === username)
            : tweets;

            return data;
        } catch (error) {
            console.error(error);
        }
    },

    postTweets: async function(sData) {
        console.log(`sData : ${sData}`);
        const {text, name, username} = sData;
        try {
            let tweet = {
                id: Date.now().toString(),
                text,
                name,
                username,
                createdAt: new Date(),
            }
            tweets.unshift(tweet);   
            return tweet;

        } catch (error) {
            console.error(error);
        }
    },

    updateTweets: async function(sData) {
        console.log(`sData : ${sData}`);
        let tweet = null;
        let userId = null;
        
        try {
            tweet = sData.tweet;
            userId = sData.userId;

            let data = tweets.find((t) => {
                if(Number(t.id) === Number(userId)) {
                    t.text = tweet.text;
                    return t;
                }
            });

            return data;

        } catch (error) {
            console.error(error);
        }

    },

    deleteTweets: async function(sData) {
        console.log(`sData : ${sData}`);
        let userId = null;

        try {
            let userId = sData.userId;
            let tweet = tweets.filter((t, idx) => {
                if(Number(t.id) === Number(userId)) {
                    tweets.splice(idx, 1);
                    return t;
                }
            })

            return tweet;

        } catch (error) {
            console.error(error);
        }
    }
}