import express from 'express';
import { body, param, validationResult } from 'express-validator';
import dataController from '../controller/DataController.js';
import { validate } from '../middleware/validator.js';
import { isAuth } from '../middleware/auth.js';


// Contract Testing: Client - Server 
// Proto - 
const router = express.Router();
const validateTweet = [
    body('text')
        .trim()
        .notEmpty()
        .isLength({min: 3})
        .withMessage('최소 3글자 이상 작성하세요.'),
    validate
];

// 여기 validate를 넣어주면 다른 라우터에서도 일일이 유효성검사를 할 때, validate함수를 작성하는 번거러움이 있으므로
// validate.js 라는 모듈을 만들어서 필요한 라우터에게 모듈을 불러오는 방식으로 해야 됨.
/* 
const validate = (req, res, next) => {
    console.log('validate() ');
    const errors = validationResult(req);
    console.log('errors() : ', errors.array());
    if(errors.isEmpty()) {
        return next();
    }
    return res.status(400).json({message: errors.array()})    
}
*/



// console.log('dataController : ', dataController.getTweets);

// GET /tweets
// GET /tweets?username=:username
router.get('/getTweets', isAuth, dataController.getTweets); // 트위터 모든 정보 가져오기

// GET /tweets/:id
router.get('/getTweets/:id', 
[
    //param('id').
], isAuth,
dataController.getTweet); // 트위터 모든 정보 가져오기

// POST /tweets
router.post('/postTweets', isAuth, validateTweet, dataController.postTweets); // 트위터 작성

// PUT /tweets/:id
router.put('/updateTweets/:id', isAuth, validateTweet, dataController.updateTweets); // 트위터 수정

// DELETE /tweets/:id
router.delete('/deleteTweets/:id', isAuth, dataController.deleteTweets); // 트위터 삭제















let tweets = [
    {
        id: 1,
        text: '드림코딩에서 강의 들으면 너무 좋으다',
        createdAt: Date.now().toString(),
        name: 'ellie',
        username: 'ellie',
        url: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-1.png',
    },
    {
        id: 2,
        text: '코딩',
        createdAt: Date.now().toString(),
        name: 'bob',
        username: 'bob'
    },
]


// GET /tweets
// GET /tweets?username=:username
/* 
router.get('/', (req, res, next) => {
    console.log('router good000')
    
    try {
        console.log('111111111');
        /* if(req.query.username !== null || req.query.username !== 'undefined'){
            // username값이 넘어옴.
            console.log('22222222');
            let resTweets = tweets.filter(function (item, index) {
                return item.username === req.query.username;
            });
            console.log('33333333333');
            res.json(resTweets);

        }else{
            res.json(tweets);        
        } 


        // solution code
        const username = req.query.username;
        const data = username 
        ? tweets.filter((t) => t.username === username)
        : tweets;

        console.log('username : ', username);
        console.log('data : ', data);

        res.status(200).json(data);

    } catch (error) {
        console.log('error: ', error);
    }
    // res.send('welcome word');
    
    //next();
});
 */


// GET /tweets/:id
router.get('/:id', (req, res, next) => {

    try {
        console.log('params: ', req.params.id);  
        
        // solution code
        const userId = req.params.id;
        const tweet = tweets.find((t) => t.id === Number(userId));
        console.log(`tweet : ${JSON.stringify(tweet)}`);

        if(tweet)  res.status(200).json(tweet);
        else res.status(404).json({ msg: `id(${userId}) is not found`});

    } catch (error) {
        console.log(error)    
    }
})


// POST /tweets
/* 
router.post('/', (req, res, next) => {
    console.log('post - tweets');
    console.log(req.body);

    /* const tweet = req.body;
    tweets.push(tweet);

    console.log('tweets : ', tweets);
    res.status(200).json(tweets); 

    console.log('res.body : ', req.body.username);
    // solution
    const {text, name, username} = req.body;

    let tweet = {
        id: Date.now().toString(),
        text,
        name,
        username,
        createdAt: new Date(),
    }

    tweets.unshift(tweet);   
    res.status(201).json(tweet);


}); */
// PUT /tweets/:id
// 업데이트 시 오류남. 확인해야됨. 22.02.13
// 오류 해결
/* 
router.put('/:id', (req, res, next) => {
    let tweet = req.body;
    let userId = req.params.id;

    console.log(`put tweet : ${JSON.stringify(tweet)}`);
    console.log(`put userId : ${userId}`);
    let data = tweets.find((t) => {
        if(Number(t.id) === Number(userId)) {
            t.text = tweet.text;
            return t;
        }
    });
    console.log(`data : ${data}`);
    res.status(201).json(data);

})
 */
// DELETE /tweets/:id
/* 
router.delete('/:id', (req, res, next) => {
    c

    let tweet = tweets.filter((t, idx) => {
        if(Number(t.id) === Number(userId)) {
            tweets.splice(idx, 1);
            return t;
        }
    })

    // console.log(`tweet : ${JSON.stringify(tweet)}`);
    // console.log(`tweets : ${JSON.stringify(tweets)}`);

    res.status(204).json(tweet);
})
 */



router.get('/tweets222222', (req, res, next) => {
    console.log('router good111')
    // res.send('welcome word');
    res.json(tweets);
    //next();
});

// router.post('/', (req, res) => {
//     tweets.push(req.body)
//     console.log(tweets);
    
//     res.json(tweets);
//     console.log('router post good222')
// })

router.post('/tweets/1', (req, res) => {
    tweets.push(req.body)
    console.log(tweets);
    
    res.json(tweets);
    console.log('router post good222')
})

export default router;
//module.exports = router;