import express from 'express';
import { body, param, validationResult } from 'express-validator';
import AuthController from '../controller/AuthController.js';
import { isAuth } from '../middleware/auth.js';
import {validate} from '../middleware/validator.js';


const router = express.Router();


// const validateAuth = [
//     ...validateSignUp,
//     body('username')
//         .trim()
//         .notEmpty()
//         .isLength({min: 3})
//         .withMessage('최소 3글자 이상 작성하세요.'),
//     body('email')
//         .trim()
//         .isEmail()
//         .withMessage('올바른 이메일 형식을 입력해주세요. ')
//         .normalizeEmail(),
//     validate
// ];

const validateCredentional = [
    body('username')
        .trim()
        .notEmpty()
        .withMessage('username should be at least 5 characters'),
    body('password')
        .trim()
        .isLength({ min: 5})
        .withMessage('password should be at least 5 characters'),
    validate
]

const validateSignUp = [
    ...validateCredentional,
    body('name').notEmpty().withMessage('name is missing'),
    body('email').isEmail().normalizeEmail().withMessage('invalid email'),
    body('url')
        .isURL()
        .withMessage('invalid url')
        .optional({ nullable: true, checkFalsy: true}), // null or '' 값 받아줌.
    validate
]




// 로그인, 회원가입 시 따로 validate를 만들어줘야됨. 나는 하나로만 했음. 
// POST /login
router.post('/login', validateCredentional, AuthController.postLogin); //  로그인

// POST /signUp
router.post('/signup', validateSignUp, AuthController.postSignUp); // 회원가입

router.get('/me', isAuth, AuthController.me);

// POST /login copy
//router.post('/me', validateAuth, AuthController.postLoginMe); // 로그인 토큰 확인


export default router;