import { validationResult } from 'express-validator';

// 밑에 주석 처리한 부분은 에러가 남. 엘리 코드에서는 안나는데, 나한데만 에러가 남..ㄷ 
// The requested module '../middleware/validator.js' does not provide an export named 'default'
export const validate = (req, res, next) => {
// export default function validate(req, res, next){
    console.log('validate() ');
    const errors = validationResult(req);
    // console.log('errors() : ', errors.array()[0].msg);
    if(errors.isEmpty()) {
        return next();
    }
    return res.status(400).json({message: errors.array()[0].msg})    
}