import jwt from 'jsonwebtoken';
import * as authModel from '../data/Auth.js';

const AUTH_ERROR = {message: 'Authentication Error'};
const jwtSecretKey = 't8@^HnEhG%LV&SQq$$LTYQdP!93UXGtG';

export const isAuth = async (req, res, next) => {
    const authHeader = req.get('Authorization');    // authheader 값 저장
    if(!(authHeader && authHeader.startsWith('Bearer '))) {
        return res.status(401).json(AUTH_ERROR);
    }
    
    const token = authHeader.split(' ')[1];
    // TODO: Make it secure
    jwt.verify(
        token,
        jwtSecretKey, 
        async (error, decoded) => {
            if(error) {
                return res.status(401).json(AUTH_ERROR);
            }
            const user = await authModel.findById(decoded.id);
            if(!user) {
                return res.status(401).json(AUTH_ERROR);
            }
            req.userId = user.id;   // req.customData 등록 가능
            next();
        }
    )
}