export default class AuthService {

  constructor (http) {
    this.http = http;
  }

  async login(username, password) {


    console.log('username : ', username);
    console.log('password : ', password);

    let response = await this.http.fetch(`/auth/login`, {
      method: 'POST',
      body: JSON.stringify({username: username, password: password}),
    });
    console.log('response: ', response);

    return {
      username: response.username,
      token: response.token,
    };
  }

  async me() {
    return {
      username: 'ellie',
      token: 'abc1234',
    };
  }

  async logout() {
    return;
  }

  async signup(username, password, name, email, url) {
    return {
      username: 'ellie',
      token: 'abc1234',
    };
  }
}
