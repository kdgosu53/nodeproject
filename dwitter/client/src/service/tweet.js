export default class TweetService {
  /* tweets = [
    {
      id: 1,
      text: '드림코딩에서 강의 들으면 너무 좋으다',
      createdAt: '2021-05-09T04:20:57.000Z',
      name: 'Bob',
      username: 'bob',
      url: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-1.png',
    },
  ]; */

  //tweets = [];

  constructor (http) {
    this.http = http;
  }
 
  // 트위터 내용 가지고 옴.
  async getTweets(username) {
    console.log('getTweets', username);

    /*
    let bb = '';
    let response = this.http.fetch('http://localhost:3300/tweets', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
      .then(res => {
        console.log('res: ', res);

        res.json().then(data => {
          bb = data;

          console.log('data ~ ');
          console.log(typeof data);
          console.log('data length: ', data.length);
          console.log(data);
          for(let i = 0; i < data.length; i++) {
            
          }

          //console.log(this.tweets);

          //return this.tweets;
          //let aa = data;
          //return aa;

            // return username
            // ? this.tweets.filter((tweet) => tweet.username === username)
            // : this.tweets;
 
        })
        .catch(err => console.log(err) )
        //console.log(res.json());
        //console.log(res.json());
        //this.tweets = res.json();
      })
      .catch(console.error);
      console.log(' bb : ', bb);
      return bb;

       */
      


      

    // // this.treets배열에 값을 받아왔는데, 렌더링이 안된다라.. 


    // return username
    //   ? this.tweets.filter((tweet) => tweet.username === username)
    //   : this.tweets;


    /* solution */
      const query = username ? `?username=${username}` : '';
      console.log('url : ', this.baseURL);
      console.log('query : ', query);
      return this.http.fetch(`/tweets/getTweets${query}`, {
        method: 'GET',
      });
  }

  async postTweet(text) {
    /* console.log('postTweet() ', text);
    const tweet = {
      id: Date.now(), 
      createdAt: new Date(),
      name: 'Ellie',
      username: 'ellie',
      text,
    };

    console.log(this.tweets); */

    console.log(`text : ${text}`);
    /* solution */
    let response = this.http.fetch(`/tweets/postTweets`, {
      method: 'POST',
      body: JSON.stringify({text, username: 'ellie', name: 'ellie' }),
    });
    
    return response;
    /* this.tweets.push(tweet);
    return tweet; */

    /* 
    return let response = this.http.fetch('http://localhost:3300/tweets', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(tweet)
    })
      .then(res => {
        console.log('res: ', res);
        res.json();
        //console.log(res.json());
        //console.log(res.json());
        //this.tweets = res.json();
      })
      .then(data => {
        console.log(data);
      })
      .catch(console.error);

    */ 
  }

  async deleteTweet(tweetId) {
      let response = this.http.fetch(`/tweets/deleteTweets/${tweetId}`, {
        method: 'DELETE',
      });
      return response;

  }

  
  // 업데이트는 되지만, 새로고침을 해야 됨. 왜인지느 ㄴ모르겟음'
  
  async updateTweet(tweetId, text) {
    let response = this.http.fetch(`/tweets/updateTweets/${tweetId}`, {
      method: 'PUT',
      headers:{ 'Content-Type': 'application/json' },
      body: JSON.stringify({ text }),
    });
    return response;
  }
}
 