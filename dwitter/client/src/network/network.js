export default class HttpClient {
    constructor(baseURL) {
        this.baseURL = baseURL;
    }

    async fetch(url, options) {
        const res = await fetch(`${this.baseURL}${url}`, {
            ...options,
            headers: {
                'Content-Type': 'application/json',
                ...options.headers,
            },
        });
        let data;
        try {
            data = await res.json();    
        } catch (error) {
            console.error(error);
        }
        
        // fetch에서는 status code가 404로 들어와도 then으로 들어오고, error로 안들어옴.
        // 그래서 밑에 코드처럼 status code를 명시하여 예외처리 작업을 함
        if(res.status > 299 || res.status < 200) {
            const message = data && data.message ? data.message : 'Something went wrong!';
            throw new Error(message);
        }
        console.log('network resutl : ', data);
        return data;
        
        
    }
}