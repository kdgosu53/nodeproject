const http = require('http');
const fs = require('fs');

// const http = require('http2');  // 모든 브라우저에서 https와 함께 적용이 됨. 개발은 http모듈로 하고, 배포 시 http2로 변경함.

//console.log(http.STATUS_CODES);
//console.log(http.METHODS);

// http서버 생성                  (requestListener 등록)
const server = http.createServer((req, res) => {
    console.log('incoming... ');

    console.log(req.headers);
    console.log(req.httpVersion);
    console.log(req.method);
    console.log('[ ' + req.url + ' ]');  // 호출 url

    // req만 해주면 호출한 브라우저에서는 계속 로딩이 됨. 이것은 서버에서 아무런 반응을 취해주지 않아서 그럼. 특정 시간이 지나면 타임아웃 에러가 남
    // res인자에 값을 써주면 해결

    // url을 이용하여 다른내용을 클라이언트에게 보내줌!
    // res.write('welcome!');
    // res.end();

    const url = req.url;

   
    res.setHeader('Content-Type', 'text/html');

    if(url === '/'){
        // 일반 텍스트 출력
        //res.write('welcome!');

        // html문서 출력
        //res.setHeader('Content-Type', 'text/html');
        // res.write('<html>');
        // res.write('<head><title> Academy </title><head>');
        // res.write('<body><h1> Welcome! </h1></body>');
        // res.write('</html>');
        const read = fs.createReadStream('./html/index.html').pipe(res);

    }else if(url === '/courses'){
        //res.write('courses!');
        //res.setHeader('Content-Type', 'text/html');
        // res.write('<html>');
        // res.write('<head><title> Academy </title><head>');
        // res.write('<body><h1> Courses! </h1></body>');
        // res.write('</html>');
        const read = fs.createReadStream('./html/courses.html').pipe(res);



    }else{
        //res.write('Not Found!');
        //res.setHeader('Content-Type', 'text/html');
        // res.write('<html>');
        // res.write('<head><title> Academy </title><head>');
        // res.write('<body><h1> Not Found! </h1></body>');
        // res.write('</html>');
        const read = fs.createReadStream('./html/not-found.html').pipe(res);


    }

    //res.end();
});

// listen을 등록해야 서버가 동작함. (포트번호)
server.listen(8080);






