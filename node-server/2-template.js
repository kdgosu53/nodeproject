const http = require('http');
const fs = require('fs');
const ejs = require('ejs');

// const http = require('http2');  // 모든 브라우저에서 https와 함께 적용이 됨. 개발은 http모듈로 하고, 배포 시 http2로 변경함.

//console.log(http.STATUS_CODES);
//console.log(http.METHODS);


const name = 'Ellie';
const courses = [
    {name: 'HTML'},
    {name: 'CSS'},
    {name: 'JS'},
    {name: 'NODE'},
    {name: 'Frontend'},
]

// 서버의 문제점 ? 
// 브라우저 클라이언트만 사용할 수 있는 서버. 
// 다른 클라이언트가 우리 서버를 사용하려면 ? 
const server = http.createServer((req, res) => {
    const url = req.url;
    res.setHeader('Content-Type', 'text/html');
    if(url === '/'){                               // key와  value값이 같으면 하나로
        ejs.renderFile('./template/index.ejs', {name})
            .then(data => res.end(data)); // 한번만 보내야 하면, end로 데이터를 보냃 수 있다. write를 안쓰고. 
    }else if(url === '/courses'){
        ejs.renderFile('./template/courses.ejs', {courses})
            .then(data => res.end(data));
    }else{
        ejs.renderFile('./template/not-found.ejs', {name})
            .then(data => res.end(data));
    }

    //res.end();
});

// listen을 등록해야 서버가 동작함. (포트번호)
server.listen(8080);






