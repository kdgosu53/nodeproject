const http = require('http');
const fs = require('fs');

const courses = [
    {name: 'HTML'},
    {name: 'CSS'},
    {name: 'JS'},
    {name: 'NODE'},
    {name: 'Frontend'},
];

// 서버는 메모리에 데이터를 보관하고 있다. 
const server = http.createServer((req, res) => {

    const url = req.url; // what ? 클라이언트가 어떤 것을 원하는지
    const method = req.method; // how ? 그것으로 어떤것을 하고 싶은지 action

    if(url === '/courses') {
        if(method === 'GET') {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(courses));
        }else if(method === 'POST'){
            const body = [];
            req.on('data', (chunk) => {
                console.log(chunk);
                body.push(chunk);
            });

            req.on('end', () => {
                // body를 다 묶음. 
                const bodyStr = Buffer.concat(body).toString();
                // 받은 json을 object형태로 parse해야됨.
                const course = JSON.parse(bodyStr);
                courses.push(course);
                console.log(course);
                res.writeHead(201);
                res.end();
                
            })
        }
    }
   
     

   
});

// listen을 등록해야 서버가 동작함. (포트번호)
server.listen(8080);






