const bcrypt = require('bcrypt');
const saltRounds = 10;  // salt길이가 길어지면, 시간이 엄청 오래걸림. 그래서 평균적으로 8~12를 선호함.
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

const password = 'adbc1234';
const hashed = bcrypt.hashSync(myPlaintextPassword, saltRounds);   // 여기 hash값을 데이터베이스에 저장. 

console.log(`password: ${myPlaintextPassword}, hashed: ${hashed}`);

const result = bcrypt.compareSync(someOtherPlaintextPassword, hashed);
console.log(`result : ${result}`);