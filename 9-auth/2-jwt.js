const jwt = require('jsonwebtoken');

const secret = 't8@^HnEhG%LV&SQq$$LTYQdP!93UXGtG'

// 담고 싶은 정보들을 담지만, 이것이 너무 커지면 네트워크 데이터를 소모할 수 있으므로, 중요한것만 담음.
// token은 영원히 유효하다.

const token = jwt.sign(
    {
        id: 'userId',
        isAdmin: true,
    }, secret,
    { expiresIn: 2 }
);

// const edited = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InVzZXJJZCIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY0NjExNDc0NX0.hQmuZzHdk91HEkwYaBHLHA7UiQP_ftG1dPa9F9-O5DY';

jwt.verify(token, secret, (error, decoded) => {
    console.log(error, decoded);
});

console.log(`token : ${token}`);